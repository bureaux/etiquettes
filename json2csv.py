#https://realpython.com/python-json/
import json, sys, os

HEADER = 'Office;Team;MemberA;MemberB;MemberC;MemberD;MemberE;MemberF;MemberG;MemberH'

def compiler_etiquettes(fichier_json, output_dir):
    with open(fichier_json, "r") as infile:
        data = json.load(infile)

    for d in data:
        # Traitement d'un bureau
        bureau    = d["libelle"]
        if " " not in bureau:
            equipe    = ' '.join(sorted(set(list(map(lambda x: x["equipe"].upper(), d["personnels"])))))
            personnes = list(map(lambda x: x["prenom"] + " "+ x["nom"].upper(), d["personnels"]))
            a_ajouter = ";" * (8 - len(personnes))
            #print(f'{d["libelle"]};{list(map(lambda x : x.nom + " " + x.prenom + ";", d["personnels"]))}')
            csvoutfilename  = os.path.join(output_dir, f"{bureau}.csv")
            latexoutfilename= os.path.join(output_dir, f"{bureau}.tex")
            with open(csvoutfilename,"w") as csvoutfile:
                print(HEADER, file=csvoutfile)        
                print(f'{bureau};{equipe};{";".join(personnes)}{a_ajouter}', file=csvoutfile)
            with open(latexoutfilename, "w") as texoutfile:
                TEX = r"""
\documentclass[a4paper,oneside]{extreport}

\usepackage[utf8]{inputenc} 
\usepackage[T1]{fontenc}

\usepackage[most]{tcolorbox}
\usepackage{graphicx}

\usepackage[margin=1cm,paper=a4paper]{geometry}

\usepackage{xcolor}
\definecolor{bleuazur}{RGB}{61,173,226}
\definecolor{rosemagenta}{RGB}{88,138,181}
\definecolor{bleusignature}{RGB}{203,93,155}

\usepackage{etoolbox}
\usepackage{datatool}
\DTLsetseparator{;}
\DTLloaddb{offices}{%s.csv}

\renewcommand{\familydefault}{\sfdefault}

\begin{document}

\pagestyle{empty}

\DTLforeach{offices}{
 \office=Office, \team=Team, \memberA=MemberA, \memberB=MemberB, \memberC=MemberC, \memberD=MemberD, \memberE=MemberE, \memberF=MemberF, \memberG=MemberG, \memberH=MemberH}{
  
  \begin{tcbraster}[raster columns=7,valign=center,box align=base,height=3cm,boxsep=0mm,beforeafter skip=0pt,raster width=14cm, raster column skip=1mm]

    \tcbox[colframe=bleuazur!75!black]{
      \rotatebox{90}{\textbf{\large{\office}}}
    }
    \tcbox[raster multicolumn=2,colframe=rosemagenta!75!black]{
      \textbf{\large{\team}}
    }
    \tcbox[raster multicolumn=4,colframe=bleusignature!75!black]{
      \begin{tabular}{l}
        \ifdefempty{\memberA}{}{\textbf{\large{\memberA}}\\}
        \ifdefempty{\memberB}{}{\textbf{\large{\memberB}}\\}
        \ifdefempty{\memberC}{}{\textbf{\large{\memberC}}\\}
        \ifdefempty{\memberD}{}{\textbf{\large{\memberD}}\\}
        \ifdefempty{\memberE}{}{\textbf{\large{\memberE}}\\}
        \ifdefempty{\memberF}{}{\textbf{\large{\memberF}}\\}
        \ifdefempty{\memberG}{}{\textbf{\large{\memberG}}\\}
        \ifdefempty{\memberH}{}{\textbf{\large{\memberH}}\\}        
      \end{tabular}
    }
  \end{tcbraster} 
}

\end{document}
"""%(bureau)
                print(TEX,file=texoutfile)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("[Erreur] Arguments manquants", file=sys.stderr)
        print(f"[Erreur] Usage:\n\t{sys.argv[0]} FICHIER_JSON OUT_DIR", file=sys.stderr)
        sys.exit(1)
    else:
        fichier_json = sys.argv[1]
        output_dir   = sys.argv[2]
        compiler_etiquettes(fichier_json, output_dir)
        sys.exit(0)
