#! /bin/bash

if [ -z "$WEBSERVICE" ]; then
    echo "URL du webservice de récupération des données non définie"
    echo "Merci de renseigner la variable d'environnement WEBSERVICE"
    exit 0
fi

OUTDIR="$PWD/outdir"
# creation si besoin
if [ ! -d "$OUTDIR" ]; then
    mkdir $OUTDIR
fi
    
# compilation fichiers tex
## batiment a
BAT_A=a.json
curl --connect-timeout 5 "$WEBSERVICE/a" -s --output $BAT_A
if [ -s "$BAT_A" ]; then
    python3 json2csv.py $BAT_A $OUTDIR
fi
## batiment b
BAT_B=b.json
curl --connect-timeout 5 "$WEBSERVICE/b" -s --output $BAT_B
if [ -s "$BAT_B" ]; then
    python3 json2csv.py $BAT_B $OUTDIR
fi
## batiment c
BAT_C=c.json
curl --connect-timeout 5 "$WEBSERVICE/c" -s --output $BAT_C
if [ -s "$BAT_C" ]; then
    python3 json2csv.py $BAT_C $OUTDIR
fi

# Compilation
cd $OUTDIR

count=`ls -1 *.tex 2>/dev/null | wc -l`
if [ $count != 0 ]; then 
    ## Compilation des étiquettes
    for i in $(ls *.tex) ; do rubber --pdf $i ; done
    ## Nettoyage des fichiers auxiliaires
    for i in $(ls *.tex) ; do rubber --clean $i ; done
fi
